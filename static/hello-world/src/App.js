import React, { useEffect, useState } from 'react';
import { invoke, Modal } from "@forge/bridge";

const modal = new Modal({
  resource: "my-modal-resource",
  onClose: (payload) => {
    console.log("onClose called with", payload);
  },
  size: "medium",
  context: {
    customKey: "custom-value",
  },
});

function App() {
  modal.open();

  const [data, setData] = useState(null);

  useEffect(() => {
    invoke('getText', { example: 'my-invoke-variable' }).then(setData);
  }, []);

  return (
    <div>
      {data ? data : 'Loading...'}
    </div>
  );
}

export default App;
