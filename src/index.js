import Resolver from '@forge/resolver';

const resolver = new Resolver();

resolver.define('getText', (req) => {
  console.log(req);

  return 'Modal should be open now';
});

export const handler = resolver.getDefinitions();
